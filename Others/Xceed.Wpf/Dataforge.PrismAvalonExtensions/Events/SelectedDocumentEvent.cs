﻿using Prism.Events;

namespace Dataforge.PrismAvalonExtensions.Events
{
    public class SelectedDocumentEvent : PubSubEvent<string>
    {
    }
}
